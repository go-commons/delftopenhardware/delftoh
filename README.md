This is the source of the Repo for the website
To see the website visit: https://delftopenhardware.nl/

# How to contribute to content
- Make a branch
- Add markdown files in existing folders or create a new project
- `cd src`
- Run `bash build.sh` to see how the new version looks
- Push and make a pull request :)

**Comment on the pages comment**


