---
meta:
  - name: description
    content: Delft Open Hardware Community
  - name: keywords
    content: hardware, design, open source, TU Delft, open science, community, meetups 
home: true
description: Simple description
title: Vuepress
heroText: Vuepress Tailwind Starter
tagline: Simple starter with Tailwind CSS
actionText: Get Started →
actionLink: /content/
features:
- title: Simplicity First
  details: Minimal setup with markdown-centered project structure helps you focus on writing.
- title: Vue-Powered
  details: Enjoy the dev experience of Vue + webpack, use Vue components in markdown, and develop custom themes with Vue.
- title: Performant
  details: VuePress generates pre-rendered static HTML for each page, and runs as an SPA once a page is loaded.
footer: MIT Licensed | Copyright © 2019-present David Couronné
---

# Delft Open Hardware
## Hi you! 👋
### Welcome to our website 0.0.3 😉

To make it easy for you to get to know us🤝, and to contribute to projects💪, we made this website.


## What is Delft Open Hardware
We like to do Open Source Hardware projects and are always up for a good laugh.

## 3rd Meetup February 2020
- [Raymond Schouten Slides and workshop materials](https://tudl1086.home.xs4all.nl/arduinoworkshop/index-arduinoworkshop.htm)
- [Micropython workshop documentation](https://docs.google.com/document/d/1xvpGJrCXErw-azy3JbZFp2p5_AyC7D5WExE5tsKmq9k/edit?usp=sharing)

## Past events
📆29 November 2019 15:00 - Update event of running projects - Blue room TU Delft Library
At this event we will tell all about the projects that are ongoing, how to help out and what to expect in the future!

📆11 October 2019 15:00 - Kickoff event - Orange room TU Delft Library
