# New project idea?

You have a great idea that you want to make tangible? To make it easy for others to join your project it is good to a Project proposal. A small written document that explains:

write an email to team@delftopenhardware.nl with the following information:
- what it is you want to build
- why it is relevant
- who is involved
- where you need help
- references to papers or media

we made it easy for you to write something by providing a template. you can find it in the drive: -> projects- project proposal.doc [here](https://drive.google.com/embeddedfolderview?id=1MDBLRiyQc7N9FRPAhdJ3-HEAfcO9J7OW#grid)
