# Great tips for great contributions 🚀

### Remember the main goal of the community 👇

> How can we build together an open source driven community in Delft that develops impactful open hardware projects that benefits students, researchers, professionals and the University? 

### Different activities you can do in the community:
- Making or sharing great tutorials to learn things like git, arduino, etc.
- Organizing community activities and events.
- Project documentation.
- Joining existing projects.
- Starting new projects.
- others you might think of....

## 1. Tips for newcomers
### Here are some things you can do get started:
- Drop your question on the chat, people will help you out.
- Join on a Friday lunch break and let us know you are joining.
- Share your background and interests in the chat room. 
- Find issues labeled with help wanted, tasks, etc. of your interest accross different repositories [here](https://gitlab.com/groups/go-commons/delftopenhardware/-/issues). 


## 2. Do's for project leads and maintainers
- Start with a project brief introduction. [Use this template it will help you to get started](https://docs.google.com/document/d/1ptJY93ptPg3U51GPdzmX062Mk1_rtBNhuNS5UbDqQes/edit). 
- [See the example of repurposing smartphones](https://drive.google.com/drive/folders/1MDBLRiyQc7N9FRPAhdJ3-HEAfcO9J7OW). 
  - Make sure that this doc is accesible so that you can have proper peer review.
  - You can later transfer it to [our project documents in the google drive](https://drive.google.com/drive/folders/1MDBLRiyQc7N9FRPAhdJ3-HEAfcO9J7OW).

### Proper project documentation and maintanance
- Make to have a README.md at the root of your folder.
  - In the README specify the current state of the project (this includes readiness to replicate, and use the source you have created).
- Open gitlab issues that specify particular problems you are facing, tasks or help needed.
  - Take advantage of [these templates](02_Templates.md) to help you document crystal clear issues.

## 3. Making suggestions, sharing ideas and documenting them properly
### 3.1 Organizational suggestions
If you want to propose activities such as a workshop, event or propose new topics for the coming meetup drop a line on the chat, or [open an issue in this repo](https://gitlab.com/go-commons/delftopenhardware/delftoh). If you open an issue that would be cool as people can respond with emojis, likes, etc.

### 3.2. Opening new issues in projects
- Request to join the particular project to the project lead in gitlab.
- Open an issue on this particular project and [use existing templates](02_Templates.md) to save you time.

