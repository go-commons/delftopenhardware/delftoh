# Issue management
Issues are a core tool to effectively get things done in open source communities. They stand for basically issues (ideas, problems, bugs, concepts) Use these **issues** boilerplates to convey your message to other community members that could help you. Feel free to add things to the template when needed.

Copy/Paste the markdown of the particular issue you are trying to tackle in your new issue.

## Issue type: Requesting for help
- Open an issue and label it with help wanted.
```md
## Problem
Briefly explain the issue that needs to be solved.

## Solution path
Do you have some idea on how to solve it? 
You might also paste some links to references 
related to the potential solution.

## Specify the skill set and help you need
Say you need someone with expertise on Arduino, Raspberrypi,
Python, 3D printing, etc. Drop a line explaining you need this
kind of support.
```

## Issue type: Specifying a design requirement or feature
- Say you have an idea for an improvement in a particular project.
- Label it as a feature request

```md
## Problem
Briefly explain the issue that needs to be solved.

## Solution path
Do you have some idea on how to solve it? 
You might also paste some links to references 
related to the potential solution.

### Detail the concept so that others understand it:
Make a sketch of your concept, present some images, 
you can also find them online, you can also link it 
to other issues related to this one.

### Write down next steps:
- Experiment one(you might want to document the 
    experiment in a specific issue)

## Specify the skill set and help you need
Say you need someone with expertise on Arduino, Raspberrypi,
Python, 3D printing, etc. Drop a line explaining you need this
kind of support.
```

## Issue type: Documenting a test or experiment
```md
## Experiment/test scope:
You can define specifications and requirements you are 
aiming for, say for instance temperature values, 
performance parameters, or simply a goal like
extruding this kind of plastic 

## Specify materials needed
- Here you might want to list things you need to develop 
the concept further including ordering components, etc. 

## Results
If you happen to have interesting results these issue can be
added to the project documentation.

```



