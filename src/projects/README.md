# Current running projects

## Centrifuge
Project lead: Jannes
Field: Medical
Status: Ongoing
Link: [Git repositories](https://gitlab.com/go-commons/delftopenhardware) 
## Fume sensor
Project lead: Jerry
Field: Environment
Status: Ongoing
Link: [Git repositories](https://gitlab.com/go-commons/delftopenhardware)
## Badge
Project lead: Suryansh
Field: Cool
Status: Ongoing
Link: [Git repositories](https://gitlab.com/go-commons/delftopenhardware)
## Circulair Solar
Project lead: Siemen
Field: Energy
Status: Just started
Link: [Git repositories](https://gitlab.com/go-commons/delftopenhardware)